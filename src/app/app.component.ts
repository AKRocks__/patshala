import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'patshala-pro2';
  listOfResponses;
  i = 0;
  constructor(private http: HttpClient) {
    this.http.get('http://localhost:4201').subscribe(response => {
      this.listOfResponses = response;
      console.log(this.listOfResponses);
    })
  }
  
  increaseCount() {
    this.i += 1;
    return this.i;
  }
}
