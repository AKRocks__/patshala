import express from 'express';
import * as fs from 'fs';

import { google } from 'googleapis';


const app = express();
const credentials = require('./credentials.json');
const SCOPE = ['https://www.googleapis.com/auth/drive'];
const TOKEN_PATH = 'token.json';
var arrOfFIles=[];

function authorize(credentials,callback) {
    const {client_secret, client_id, redirect_uris} = credentials.installed;
    const oAuth2Client = new google.auth.OAuth2(
        client_id, client_secret, redirect_uris[0]);

    // Checking if we have previously stored a token.
    fs.readFile(TOKEN_PATH, (err, token) => {
        //if (err) return getAccessToken(oAuth2Client); //Generating token (To Genetate Token uncomment getAccessTokent() function)
        oAuth2Client.setCredentials(JSON.parse(token));
        callback(oAuth2Client);
    });
}

function listFiles(auth) {
    const drive = google.drive({ version: 'v3', auth }); //Creating drive variable to access the google drive

    
    drive.files.list({
        pageSize: 100, // Max Number of Output Files... 
        //fields: 'files(id,name)', // Fields in the output.......
        fields: 'nextPageToken, files(id, name, webViewLink)', // Uncomment this line if webLink of the file is needed and delete the above attribute 'fields'
        // 'nextPageToken' is used to jump to next page
        q: "'1aVDLx2FJVLfB_etqfl-1cRM3ADcfvswg' in parents",   // Giving the folder id whose files are to be listed (Remove this if you want to list the whole drive)
        // 1aVDLx2FJVLfB_etqfl-1cRM3ADcfvswg is Folder id.....
    }, (err, res) => {
        if (err) return console.log('The API returned an error: ' + err);
        const files = res.data.files;
            
        if (files.length) {
          for( var i in files){
              arrOfFIles.push(files[i]['webViewLink']);
          }          
        } else {
            console.log('No Files Found');
        }
    })  
}
authorize(credentials, listFiles);
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin','*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    res.header('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PUT, DELETE');
    if ('OPTIONS' == req.method) {
        res.sendStatus(200);
    } else {
        console.log(`${req.ip} ${req.method} ${req.url}`);
        next();
    }
});

app.use(express.json());

app.get('/', (req, res) => {
    res.send(arrOfFIles);
})
app.listen(4201, '127.0.0.1', function () {
    console.log('Server Listening on 4201');
});